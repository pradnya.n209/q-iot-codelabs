#!/bin/bash

clear

GS_BUCKET=$1

now=`date --date='+5 hour 30 minutes' '+%d/%b/%y %r'`
echo -e "starting time : $now "

echo -e "Installing Required Packages For Dataflow"

project_Id=$(echo "${BQ_PROJECT_ID,,}")

dataset_name=$(echo "${BQ_DATASET_NAME,,}")

sudo pip install apache_beam[gcp] -y

sudo pip install google-cloud-bigquery==0.25.0 -y

sudo pip install google-cloud-dataflow==2.4.0 -y

sudo gsutil -m cp -r gs://iot-workshop-resources/Final-workshop-resources/PubSub_To_BigQuery_Multi.py PubSub_To_BigQuery_Multi.py

sudo nohup python PubSub_To_BigQuery_Multi.py --input_topic projects/$project_Id/topics/dataflow_test   \
                                        --output_table_temp $project_Id:$dataset_name.Temp_Sensor_Type \
                                        --output_table_light $project_Id:$dataset_name.Light_Sensor_Type \
                                        --output_table_door $project_Id:$dataset_name.Door_Sensor_Type \
                                         --runner DataflowRunner \
                                         --project $project_Id \
                                         --temp_location $GS_BUCKET \
                                         --streaming


