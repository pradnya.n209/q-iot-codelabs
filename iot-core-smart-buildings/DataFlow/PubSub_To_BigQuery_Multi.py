from __future__ import absolute_import

import logging
import argparse
import apache_beam as beam
import apache_beam.transforms.window as window
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.options.pipeline_options import StandardOptions
import json

'''{"Temperature_Living": "20.1", "Temperature_Bedroom": "21.03", "Temperature_Kitchen": "20.86", "Humidity": "33.59", "Light_Living": "0.0", "Light_Bedroom": "0", "Light_kitchen": "1", "CO2": "505.5", "HumidityRatio": "0.004890424", "Occupancy_Living": "0", "Occupancy_Bedroom": "0", "Occupancy_Kitchen": "0", "Door_Sensor": "0"}'''

output_table='katerra:katerra_workshop.Temp_Sensor_Type'
'''Normalize pubsub string to json object'''
# Lines look like this:
  # {'datetime': '2017-07-13T21:15:02Z', 'mac': 'FC:FC:48:AE:F6:94', 'status': 1}
def parse_pubsub(line):
    record = json.loads(line)
    return (record['device_id'],record['Temperature_Living'],record['Temperature_Bedroom'],record['Temperature_Kitchen'],record['Temperature'],record['Humidity'],record['timecollected'])

def parse_pubsub_light(line):
    record = json.loads(line)
    return (record['device_id'],record['Light_Living'],record['Light_Bedroom'],record['Light_kitchen'],record['Light'],record['CO2'],record['HumidityRatio'],record['timecollected'])

def parse_pubsub_door(line):
    record = json.loads(line)
    return (record['device_id'],record['Occupancy_Living'],record['Occupancy_Bedroom'],record['Occupancy_Kitchen'],record['Occupancy'],record['Door_Sensor'],record['Sound'],record['timecollected'])

def run(argv=None):
  """Build and run the pipeline."""

  parser = argparse.ArgumentParser()
  parser.add_argument(
      '--input_topic', required=True,
      help='Input PubSub topic of the form "/topics/<PROJECT>/<TOPIC>".')
  known_args, pipeline_args = parser.parse_known_args(argv)

  with beam.Pipeline(argv=pipeline_args) as p:
    # Read the pubsub topic into a PCollection.
    lines = ( p | 'Read Input from pubsub' >> beam.io.ReadStringsFromPubSub(known_args.input_topic) 
            )
    print(lines)
    Temps = lines
    light = lines
    Door  = lines

    ( Temps
        | 'JSON Function fire temp' >> beam.Map(parse_pubsub)  
        | 'Mapping Input for Temps' >> beam.Map(lambda (device_id,Temperature_Living,Temperature_Bedroom,Temperature_Kitchen,Temperature,Humidity,timecollected): {'device_id': device_id, 'Temperature_Living': Temperature_Living, 'Temperature_Bedroom': Temperature_Bedroom, 'Temperature_Kitchen': Temperature_Kitchen,'Temperature':Temperature, 'Humidity': Humidity,'timecollected':timecollected})
        | 'BQ Table Write Temps' >> beam.io.WriteToBigQuery(
                    'smart_building.Temp_Sensors_data',
                    schema='device_id:STRING,Temperature_Living:FLOAT, Temperature_Bedroom:FLOAT, Temperature_Kitchen:FLOAT,Temperature:FLOAT,Humidity:FLOAT,timecollected:TIMESTAMP',
                    create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                    write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND)
            )

    ( light
        | 'JSON Function fire light' >> beam.Map(parse_pubsub_light)  
        | 'Mapping Input for light' >> beam.Map(lambda (device_id, Light_Living, Light_Bedroom, Light_kitchen,Light, CO2,HumidityRatio,timecollected): {'device_id': device_id, 'Light_Living': Light_Living, 'Light_Bedroom': Light_Bedroom, 'Light_kitchen': Light_kitchen,'Light':Light, 'CO2': CO2,'HumidityRatio':HumidityRatio,'timecollected':timecollected})
        | 'BQ Table Write light' >> beam.io.WriteToBigQuery(
                    'smart_building.luminocity_Sensors_data',
                    schema=' device_id:STRING, Light_Living:FLOAT, Light_Bedroom:FLOAT, Light_kitchen:FLOAT,Light:FLOAT,CO2:FLOAT,HumidityRatio:FLOAT,timecollected:TIMESTAMP',
                    create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                    write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND)
            )

    ( Door
        | 'JSON Function fire door' >> beam.Map(parse_pubsub_door)  
        | 'Mapping Input for door' >> beam.Map(lambda (device_id, Occupancy_Living, Occupancy_Bedroom, Occupancy_Kitchen,Occupancy, Door_Sensor,Sound,timecollected): {'device_id': device_id, 'Occupancy_Living': Occupancy_Living, 'Occupancy_Bedroom': Occupancy_Bedroom, 'Occupancy_Kitchen': Occupancy_Kitchen, 'Occupancy': Occupancy,'Door_Sensor':Door_Sensor,'Sound':Sound,'timecollected':timecollected})
        | 'BQ Table Write door' >> beam.io.WriteToBigQuery(
                    'smart_building.Occupancy_Sensors_data',
                    schema=' device_id:STRING, Occupancy_Living:INTEGER, Occupancy_Bedroom:INTEGER, Occupancy_Kitchen:INTEGER, Occupancy:INTEGER,Door_Sensor:INTEGER,Sound:FLOAT,timecollected:TIMESTAMP',
                    create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                    write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND)
            )

if __name__ == '__main__':
  logging.getLogger().setLevel(logging.INFO)
  run()




