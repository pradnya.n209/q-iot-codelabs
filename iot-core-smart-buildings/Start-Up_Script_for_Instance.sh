#!/bin/bash

# Run commands that will install required software's

sudo chmod 777 data

sudo chmod +x initialsoftware.sh

echo -e "chmod +x initialsoftware.sh command as run" >>log_$Instance_Name.txt

./initialsoftware.sh

echo -e "./initialsoftware.sh command as run" >>log_$Instance_Name.txt

sudo chmod +x generate_keys.sh

echo -e "chmod +x generate_keys.sh command as run" >>log_$Instance_Name.txt

./generate_keys.sh

echo -e "./generate_keys.sh command as run" >>log_$Instance_Name.txt

cat ../../.ssh/ec_public.pem >>ssh_sensor.pem

echo -e "Public Key of Device"

cat ssh_sensor.pem
