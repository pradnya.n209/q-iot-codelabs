#!/bin/bash

PROJECT_ID=$1
Region='us-central1'
No_device=12

gcloud config set project $PROJECT_ID

echo $PROJECT_ID
echo $Region


#Create Registry

REGISTRY_ID='iot-smart-stream'

gcloud iot registries create $REGISTRY_ID \
    --project=$PROJECT_ID \
    --region=$Region \
    --event-notification-config=topic=iot-smart-building \
    --enable-http-config \
    --enable-mqtt-config



#printenv

for((i=1;i<=$No_device;i++))
do
	device_id='Stream-data-'$i
	echo $device_id
	gcloud iot devices create $device_id \
			  --project=$PROJECT_ID \
			  	    --region=$Region \
				    	      --registry=$REGISTRY_ID \
					      	        --public-key path=ssh_sensor.pem,type=es256
	echo "Device $device_id registered successfully"
done
