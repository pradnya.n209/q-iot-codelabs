#!/bin/bash

Project=$1
Registry=$2

Instance_Name=$(echo "${HOSTNAME,,}")

for((i=1;i<=1;i++))
do
 python DeviceSimulatorScript.py --project_id=$Project --registry_id=$Registry --device_Id=$Instance_Name-$i --json_data_file=data/D$i.json --private_key_file=../.ssh/ec_private.pem &

done