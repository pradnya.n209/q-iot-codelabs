#!/bin/bash

Project_ID=$(echo "${PROJECT_ID,,}")

sudo gcloud config set project $Project_ID

isDatasetExists=0

DatasetName=("smart_building")

#bq mk --table --schema loc_id:STRING, long:STRING, lat:STRING, build_id:STRING, floor_id:STRING, block_id:STRING,device_id:STRING iot-workshop-226708:iot_workshop_demo.Building_Metadata

#bq


#Check if Dataset Exists

bq_safe_mk() {

	for((i=0;i<=1;i++))
	do
    dataset=${DatasetName[$i]} 
    echo $dataset
    exists=$(bq ls -d | grep -w $dataset)
    if [ -n "$exists" ]; then
       echo "Not creating $dataset since it already exists deleting tables"
       bq_delete_tables $dataset
    else
       echo "Creating $dataset"
       bq mk $dataset
    fi
done
}


bq_delete_tables(){
   
   dataset_nm=$1
   echo $dataset_nm
   datas=${dataset_nm:0:2}
   bq query "Select table_id from $Project_ID:$dataset_nm.__TABLES__ Group by 1" > $dataset_nm_table.tab
  sudo cat $dataset_nm_table.tab | grep -e $datas | sed 's/|//g' | sed 's/" "//g'| sed 's/[^a-zA-Z0-9_]//g'| sed 's/"table_id"//g'> $dataset_nm_final.txt
  sudo cat $dataset_nm_final.txt | while read table_id
  do 
  bq query --use_legacy_sql=false "drop table \`$Project_ID.$dataset_nm.$table_id\`"
  done

}


bq_safe_mk

#Stream-data-1
 bq load --quote "" --allow_quoted_newlines --allow_jagged_rows --skip_leading_rows=1 --ignore_unknown_values --source_format=CSV --field_delimiter=',' smart_building.master_data master_data.csv loc_id:STRING,long:STRING,lat:STRING,build_id:STRING,floor_id:STRING,block_id:STRING,device_id:STRING


