#!/bin/bash

Project=$1
Registry=$2
No_device=12

Instance_Name=$(echo "${HOSTNAME,,}")

for((i=1;i<=$No_device;i++))
do
	 device_id='Stream-data-'$i
	 python DeviceSimulatorScript.py --project_id=$Project --registry_id=$Registry --device_id=$device_id --json_data_file=data/D$i.json --private_key_file=../../.ssh/ec_private.pem &

 done
