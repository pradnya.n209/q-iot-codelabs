/*----- START CHART SCRIPT ----- 
google.charts.load('current', { 'packages': ['map'] });
google.charts.setOnLoadCallback(drawMap);*/

const average = arr => arr.reduce( ( p, c ) => p + c, 0 ) / arr.length;
var roomCount = 0;
/*function drawMap() {
    var data = google.visualization.arrayToDataTable([
        ['Lat', 'Long', 'Name'],
        [56.130367, -106.346771, 'Habitat-1501'],
        [36.758721, -95.933272, 'Whitehouse-1788']
    ]);
    var options = {
        showTooltip: true,
        showInfoWindow: true
    };
    var map = new google.visualization.Map(document.getElementById('map-div'));
    var infoWindow = 



    map.draw(data, options);
};*/
/*var locations = [
  ['Canada', 56.130367,-106.346771, 'address 1'],
  ['USA', 36.758721 -95.933272, 'address 2']
  ];


function drawChart(marker) {

        // Create the data table.
        var data = google.visualization.arrayToDataTable([
          ['Label', 'Value'],
          ['Temperature',20.75 ],
          ['Luminocity', 450],
          ['Occupancy', 40],
          ['Sound',280]
        ]);

        
        // Set chart options
        var options = {
          width: 400, height: 120,
          redFrom: 90, redTo: 100,
          yellowFrom:75, yellowTo: 90,
          minorTicks: 5
        };

                       
        var node1 = document.createElement('div'),
            node = document.createElement('div'),
            parentDiv =document.createElement('div'),
            infoWindow  = new google.maps.InfoWindow(),
            chart = new google.visualization.Gauge(node1);
node.innerHTML= "<div style='width:400;height:800;'><h5>Location:Habitat-1501(Canada)</h5><br><img src='asset/img/giphy.gif' width=400px height=100px></img><br><h5>Sensors Reading..</h5></div>";
var div= node.firstChild;

        parentDiv.appendChild(node);
        parentDiv.appendChild(node1);
            
          chart.draw(data, options);
          setInterval(function() {
          data.setValue(0, 1, 2 + Math.round(2 * Math.random()));
          chart.draw(data, options);
        }, 13000);
        setInterval(function() {
          data.setValue(1, 1, 3 + Math.round(4 * Math.random()));
          chart.draw(data, options);
        }, 5000);
        setInterval(function() {
          data.setValue(2, 1, 1 + Math.round(2 * Math.random()));
          chart.draw(data, options);
        }, 26000);
            infoWindow.setContent(parentDiv);
            infoWindow.open(marker.getMap(),marker);
      }


function initialize() {

    var data = google.visualization.arrayToDataTable([
        ['Lat', 'Long', 'Name'],
        [56.130367, -106.346771, 'Habitat-1501'],
        [36.758721, -95.933272, 'Whitehouse-1788']
    ]);
    var options = {
        showTooltip: true,
        showInfoWindow: true
    };
    var map = new google.visualization.Map(document.getElementById('map-div'));

    var marker1 = new google.maps.Marker({
        position: mapOptions.center,
        map: map
    });

    /*var mapOptions = {
      center: new google.maps.LatLng(56.130367,-106.346771),
      zoom: 10,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var mapOptions1 = {
      center: new google.maps.LatLng(36.758721,-95.933272),
      zoom: 10,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    
    var map = new google.maps.Map(document.getElementById("map_canvas"),
        mapOptions);

    var map1 = new google.maps.Map(document.getElementById("map_canvas"),
        mapOptions1);

    var marker1 = new google.maps.Marker({
        position: mapOptions.center,
        map: map
    });

    var marker2 = new google.maps.Marker({
        position: mapOptions1.center,
        map: map1
    });
    
    google.maps.event.addListener(marker1, 'click', function() {
      drawChart(this);
    }); 
  }*/
  
   
  function drawChart(marker,loc,img) {

        // Create the data table.
        var data = google.visualization.arrayToDataTable([
          ['Label', 'Value'],
          ['Temperature',20.75 ],
          ['Luminocity', 450],
          ['Occupancy', 40],
          ['Sound',280]
        ]);

        
        // Set chart options
        var options = {
          width: 400, height: 120,
          redFrom: 90, redTo: 100,
          yellowFrom:75, yellowTo: 90,
          minorTicks: 5
        };

                       
        var node1 = document.createElement('div'),
            node = document.createElement('div'),
            node2= document.createElement('div'),
            parentDiv =document.createElement('div'),
            infoWindow  = new google.maps.InfoWindow(),
            chart = new google.visualization.Gauge(node1);
node.innerHTML= "<div style='width:400;height:800;'><h5>"+loc+"</h5><br><img src='asset/img/"+img+"' width=400px height=100px></img><br><h5>Sensors Reading..</h5></div><br>";
var div= node.firstChild;

node2.innerHTML="<div style='width:400;height:800;'><h5>Total Devices Connected &nbsp;<span style='background-color: #7CFC00'>"+roomCount+"</span></h5></div>"

        parentDiv.appendChild(node);
        parentDiv.appendChild(node1);
        parentDiv.appendChild(node2);
            
          chart.draw(data, options);
          setInterval(function() {
          data.setValue(0, 1, 2 + Math.round(2 * Math.random()));
          chart.draw(data, options);
        }, 13000);
        setInterval(function() {
          data.setValue(1, 1, 3 + Math.round(4 * Math.random()));
          chart.draw(data, options);
        }, 5000);
        setInterval(function() {
          data.setValue(2, 1, 1 + Math.round(2 * Math.random()));
          chart.draw(data, options);
        }, 26000);
            infoWindow.setContent(parentDiv);
            infoWindow.open(marker.getMap(),marker);
      }
  

  var locations = [
      ['Habitat-1501,Canada',56.130367,-106.346771, 4,'giphy.gif'],
      ['Whitehouse-1788,USA',36.758721,-95.933272, 5,'giphy_usa.gif']
    ];

    var map = new google.maps.Map(document.getElementById('map_canvas'), {
      zoom: 2,
      center: new google.maps.LatLng(56.130367,-106.346771),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });


      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          /*infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);*/
          var loc=locations[i][0];
          var img=locations[i][4];
          drawChart(marker,loc,img);
        }
      })(marker, i));
    }



google.charts.load('current', {'packages':['gauge']});
/* ---- END CHART SCRIPT ----- */

$(document).ready(function () {
    var statData = {
        roomsOnline: 0,
        humidex: 0,
        luminocity: 0,
        soundLevel: 0,
        humidexAvg: 0,
        temp: 0
    }
    var globalHumidex = [];
    fillStatData = () => {
        console.log(statData);
        for (let i in statData) {
            $(`#${i}`).text(statData[i]);
        }
    }
    
    var temperatureChart = new Chart($("#MaintemperatureChart"), {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: 'Habitat-1501',
            data: [],
            borderColor: 'wheat',
            borderWidth: 3
        },{label: 'Whitehouse-1788',
            data: [],
            borderColor: 'purple',
            borderWidth: 3}]
    },
    options: {
        responsive: true,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                },
                scaleLabel: {
                     display: true,
                     labelString: 'Temperature',
                     fontSize: 20 
                  }
            }]
            
        }
    }
});
    var dataPointsLimit=10

    function fillmainchart(devicedata){
      var CAN1=parseFloat(devicedata['Stream-data-1']['Temperature']);
      var CAN2=parseFloat(devicedata['Stream-data-2']['Temperature']);
      var CAN3=parseFloat(devicedata['Stream-data-3']['Temperature']);
      var CAN4=parseFloat(devicedata['Stream-data-4']['Temperature']);
      var CAN5=parseFloat(devicedata['Stream-data-5']['Temperature']);
      var CAN6=parseFloat(devicedata['Stream-data-6']['Temperature']);

      var CAN_Temp_Avg=parseFloat((CAN1+CAN2+CAN3+CAN4+CAN5+CAN6)/6).toFixed(2);

      var USA1=parseFloat(devicedata['Stream-data-7']['Temperature']);
      var USA2=parseFloat(devicedata['Stream-data-8']['Temperature']);
      var USA3=parseFloat(devicedata['Stream-data-9']['Temperature']);
      var USA4=parseFloat(devicedata['Stream-data-10']['Temperature']);
      var USA5=parseFloat(devicedata['Stream-data-11']['Temperature']);
      var USA6=parseFloat(devicedata['Stream-data-12']['Temperature']);

      var USA_Temp_Avg=parseFloat((USA1+USA2+USA3+USA4+USA5+USA6)/6).toFixed(2);

    if (temperatureChart.data.datasets[0].data.length > dataPointsLimit) {
        temperatureChart.data.datasets[0].data.splice(0, 1);
        temperatureChart.data.labels.splice(0, 1);
    }
    temperatureChart.data.datasets[0].data.push(CAN_Temp_Avg);
    temperatureChart.update();

    if (temperatureChart.data.datasets[1].data.length > dataPointsLimit) {
        temperatureChart.data.datasets[1].data.splice(0, 1);
        temperatureChart.data.labels.splice(0, 1);
    }
    temperatureChart.data.datasets[1].data.push(USA_Temp_Avg);
    temperatureChart.update();
      

    }
    
    fillStatData();
    db.ref('devices').on('value', (snapshot) => {
        var locationSnapshot = snapshot.val();
        
        roomCount = 0;
        let tempArr = [];
        let humidexArr = [];
        let luminocityArr = [];
        let sound = [];

        

        for (let i in locationSnapshot) {
            console.log(parseFloat(locationSnapshot[i]['Light']))
            roomCount++;
            humidexArr.push(calculateHumidex(parseFloat(locationSnapshot[i]['Temperature']), parseFloat(locationSnapshot[i]['Humidity'])));
            luminocityArr.push(parseFloat(locationSnapshot[i]['Light']));
            tempArr.push(parseFloat(locationSnapshot[i]['Temperature']));
            sound.push((locationSnapshot[i]['Sound']));
        }
        statData['roomsOnline'] = roomCount;
        statData['humidex'] = parseFloat(average(humidexArr)).toFixed(2);
        statData['luminocity'] = parseFloat(average(luminocityArr)).toFixed(2);
        globalHumidex.push(statData['humidex']);
        statData['humidex'] = average(globalHumidex);
        statData['temp'] = parseFloat(average(tempArr)).toFixed(2);
        statData['soundLevel']= average(sound);

        fillStatData();
        fillmainchart(locationSnapshot);
        
    });

     

    
    

    
});