function calculateHumidex(temp, humidity) {
    let kelvin = temp + 273;
    let eTs = Math.pow(10, ((-2937.4 / kelvin) - 4.9283 * Math.log(kelvin) / Math.LN10 + 23.5471));
    let eTd = eTs * humidity / 100;
    let hx = Math.round(temp + ((eTd - 10) * 5 / 9));
    console.log(hx);
    return hx;
}