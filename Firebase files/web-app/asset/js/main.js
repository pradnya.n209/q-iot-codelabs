$(document).ready(function () {

    var listener = null;
    var locationSnapshot=null;
    var currentDeviceName = null;
    db.ref('locations').once('value').then((snapshot) => {
        locationSnapshot = snapshot.val();
        $("#tree-data").html(getNestedHtml(locationSnapshot));
        $('.collapsible').collapsible();
    });

    $("#tree-data").on('click', '.devices', function (e) {
        let deviceName = $(this).attr('data-name');
        clearChartData();
        getDeviceData(deviceName);
    });

    getDeviceData = (deviceName) => {
        if(currentDeviceName && listener){
            db.ref(`devices/${currentDeviceName}`).off('value', listener);
        }
        listener = db.ref(`devices/${deviceName}`).on('value', (snapshot) => {
            let deviceSnapshot = snapshot.val();
            currentDeviceName = deviceName;
            fillStatData(deviceSnapshot);
            fillChartData(deviceSnapshot);
            fillTableData(deviceSnapshot);
        });
    }

    
    
    fillTableData = (deviceData) => {
        let tempThreshold = 27;
        let luminosityThreshold = 500;
        let occupancyThreshold = 0;

        $("#living-temperature").html(deviceData['Temperature_Living'] > tempThreshold ? `<span class="red-text">${deviceData['Temperature_Living']}</span>` : `<span class="green-text">${deviceData['Temperature_Living']}</span>`);
        $("#kitchen-temperature").html(deviceData['Temperature_Kitchen'] > tempThreshold ? `<span class="red-text">${deviceData['Temperature_Kitchen']}</span>` : `<span class="green-text">${deviceData['Temperature_Kitchen']}</span>`);
        $("#bed-temperature").html(deviceData['Temperature_Bedroom'] > tempThreshold ? `<span class="red-text">${deviceData['Temperature_Bedroom']}</span>` : `<span class="green-text">${deviceData['Temperature_Bedroom']}</span>`);

        $("#living-luminosity").html(deviceData['Light_Living'] > luminosityThreshold ? `<span class="red-text">${deviceData['Light_Living']}</span>` : `<span class="green-text">${deviceData['Light_Living']}</span>`);
        $("#kitchen-luminosity").html(deviceData['Light_kitchen'] > luminosityThreshold ? `<span class="red-text">${deviceData['Light_kitchen']}</span>` : `<span class="green-text">${deviceData['Light_kitchen']}</span>`);
        $("#bed-luminosity").html(deviceData['Light_Bedroom'] > luminosityThreshold ? `<span class="red-text">${deviceData['Light_Bedroom']}</span>` : `<span class="green-text">${deviceData['Light_Bedroom']}</span>`);

        $("#living-occupancy").html(deviceData['Occupancy_Living'] <= occupancyThreshold ? `<span class="red-text">${deviceData['Occupancy_Living']}</span>` : `<span class="green-text">${deviceData['Occupancy_Living']}</span>`);
        $("#kitchen-occupancy").html(deviceData['Occupancy_Kitchen'] <= occupancyThreshold ? `<span class="red-text">${deviceData['Occupancy_Kitchen']}</span>` : `<span class="green-text">${deviceData['Occupancy_Kitchen']}</span>`);
        $("#bed-occupancy").html(deviceData['Occupancy_Bedroom'] <= occupancyThreshold ? `<span class="red-text">${deviceData['Occupancy_Bedroom']}</span>` : `<span class="green-text">${deviceData['Occupancy_Bedroom']}</span>`);
        console.log(deviceData);
    }

    fillChartData = (deviceData) => {
        fillTemperatureChart(deviceData['Temperature']);
        fillCo2Chart(deviceData['CO2']);
        fillHumidityChart(deviceData['Humidity']);
        fillLuminocityChart(deviceData['Light']);
    }

    fillStatData = (deviceData) => {
        $("#doorSensor").text(deviceData['Door_Sensor'] == 0 ? 'Closed':'Open');
        $("#abmientLights").text(deviceData['Light']);
        let humidex = calculateHumidex(parseFloat(deviceData['Temperature']), parseFloat(deviceData['Humidity']))
        $("#humidexLevel").text(humidex > 10 ? 'Comfortable' : 'Un-comfortable');
        $("#icnHumidityLevel").text(humidex > 10 ? 'sentiment_very_satisfied' : 'sentiment_very_dissatisfied');
        $("#humidexIndex").text(humidex);
        $("#temperature").text(`${deviceData['Temperature']} C`);
        $("#lightStatus").text(deviceData['Light'] > 0 ? 'On':'Off');
        // $("#soundLevel").text(``);
        $("#occupancy").text(deviceData['Occupancy'] == 1 ? 'Occupied' : 'Un-occupied');
        $("#icnOccupancy").text(deviceData['Occupancy'] == 1 ? 'people' : 'block');
    }

    getNestedHtml = (locationData) => {
        let nestedHtml = ``;
        nestedHtml += `${getNestedContent(locationData, 0)}`;
        return nestedHtml;
    }

    getNestedContent = (data, index) => {
        let heirarchy = [
            {
                'name': 'locations',
                'id': 'location_id'
            },
            {
                'name': 'buildings',
                'id': 'building_id'
            },
            {
                'name': 'floors',
                'id': 'floor_id'
            },
            {
                'name': 'blocks',
                'id': 'block_id'
            },
            {
                'name': 'devices',
                'id': 'device_id'
            }
        ];
        let type = heirarchy[index]['name'];
        let typeIndex = heirarchy[index]['id'];

        let innerHtml = ``;
        let initDevice = null;
        for (let i in data) {
            if(type == 'devices' && initDevice == null){
                initDevice = data[i][typeIndex];
                // getDeviceData(initDevice);
            }
            innerHtml += `
            <li class="bold">
                <a class="collapsible-header waves-effect waves-teal ${type}" data-name="${data[i][typeIndex]}"><i class="material-icons">chevron_right</i> ${data[i][typeIndex]}</a>
                <div class="collapsible-body">
                    <ul>
                        ${heirarchy[index + 1] === undefined ? '' : getNestedContent(data[i][heirarchy[index + 1]['name']], (index + 1))}
                    </ul>
                </div>
            </li>
            `;
        }
        let nestedContent = `
        <li class="no-padding">
            <ul class="collapsible collapsible-accordion">
                ${innerHtml}
            </ul>
        </li>
        `;
        return nestedContent;
    }

});