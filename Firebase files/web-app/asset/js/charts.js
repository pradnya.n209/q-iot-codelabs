var temperatureChart = new Chart($("#temperatureChart"), {
    type: 'line',
    data: {
        labels: [],
        datasets: [
            {
                label: 'Temperature',
                data: [],
                // backgroundColor: "#ffbdbd",
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            },
            {
                // label: 'Threshold',
                fill: false,
                data: [27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27],
                pointRadius: 0,
                borderColor: ['#B22222']
            }
        ],
    },
    options: {
        responsive: true,
        legend: {
            display: false
        },
        title: {
            display: true,
            text: 'Temperature'
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

var co2Chart = new Chart($("#co2Chart"), {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: 'CO2',
            data: [],
            // backgroundColor: '#c9c9ff',
            borderColor: "#77f",
            borderWidth: 1
        },
        {
            // label: 'Threshold',
            fill: false,
            data: [850,850,850,850,850,850,850,850,850,850,850,850,850,850,850,850,850,850,850,850,850,850,850,850,850,850,850,850,850,850,850],
            pointRadius: 0,
            borderColor: ['#B22222']
        }
    ]
    },
    options: {
        responsive: true,
        legend: {
            display: false
        },
        title: {
            display: true,
            text: 'CO2'
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});


var humidityChart = new Chart($("#humidityChart"), {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: 'Humidity',
            data: [],
            // backgroundColor: "#a7f779",
            borderColor: "#399005",
            borderWidth: 1
        },
        {
            // label: 'Threshold',
            fill: false,
            data: [28,28,28,28,28,28,28,28,28,28,28,28,28,28,28,28,28,28,28,28,28,28,28,28,28,28,28,28,28,28,28],
            pointRadius: 0,
            borderColor: ['#B22222']
        }
    ]
    },
    options: {
        responsive: true,
        legend: {
            display: false
        },
        title: {
            display: true,
            text: 'Humidity'
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

var luminocityChart = new Chart($("#luminocityChart"), {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: 'Luminocity',
            data: [],
            // backgroundColor: "#f1cbff",
            borderColor: "#d474f7",
            borderWidth: 1
        },
        {
            // label: 'Threshold',
            fill: false,
            data: [500,500,500,500,500,500,500,500,500,500,500,500,500,500,500,500,500,500,500,500,500,500,500,500,500,500,500,500,500,500,500],
            pointRadius: 0,
            borderColor: ['#B22222']
        }
    ]
    },
    options: {
        responsive: true,
        legend: {
            display: false
        },
        title: {
            display: true,
            text: 'Luminocity'
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

var dataPointsLimit = 5;
function fillTemperatureChart(temperature) {
    if (temperatureChart.data.datasets[0].data.length > dataPointsLimit) {
        temperatureChart.data.datasets[0].data.splice(0, 1);
        temperatureChart.data.labels.splice(0, 1);
    }
    temperatureChart.data.datasets[0].data.push(temperature);
    temperatureChart.data.labels.push("");
    temperatureChart.update();
}

function fillCo2Chart(co2) {
    if (co2Chart.data.datasets[0].data.length > dataPointsLimit) {
        co2Chart.data.datasets[0].data.splice(0, 1);
        co2Chart.data.labels.splice(0, 1);
    }
    co2Chart.data.datasets[0].data.push(co2);
    co2Chart.data.labels.push("");
    co2Chart.update();
}

function fillHumidityChart(humidity) {
    if (humidityChart.data.datasets[0].data.length > dataPointsLimit) {
        humidityChart.data.datasets[0].data.splice(0, 1);
        humidityChart.data.labels.splice(0, 1);
    }
    humidityChart.data.datasets[0].data.push(humidity);
    humidityChart.data.labels.push("");
    humidityChart.update();
}

function fillLuminocityChart(luminocity) {
    if (luminocityChart.data.datasets[0].data.length > dataPointsLimit) {
        luminocityChart.data.datasets[0].data.splice(0, 1);
        luminocityChart.data.labels.splice(0, 1);
    }
    luminocityChart.data.datasets[0].data.push(luminocity);
    luminocityChart.data.labels.push("");
    luminocityChart.update();
}

function clearChartData() {
    temperatureChart.data.datasets[0].data = [];
    temperatureChart.data.labels = [];
    temperatureChart.update();

    co2Chart.data.datasets[0].data = [];
    co2Chart.data.labels = [];
    co2Chart.update();

    humidityChart.data.datasets[0].data = [];
    humidityChart.data.labels = [];
    humidityChart.update();

    luminocityChart.data.datasets[0].data = [];
    luminocityChart.data.labels = [];
    luminocityChart.update();
}