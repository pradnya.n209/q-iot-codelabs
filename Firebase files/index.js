var express = require('express')
// var cors = require('cors')
var app = express()
// app.use(cors())

const functions = require('firebase-functions');
const admin = require('firebase-admin');
// const bigquery = require('@google-cloud/bigquery')();
const cors = require('cors')({ origin: true });
admin.initializeApp(functions.config().firebase);

const db = admin.database();

/**
 * Receive data from pubsub, then 
 * Write telemetry raw data to bigquery
 * Maintain last data on firebase realtime database
 */
exports.receiveTelemetry = functions.pubsub
  .topic('iot-smart-building')
  .onPublish((message, context) => {
    const attributes = message.attributes;
    const payload = message.json;

    const deviceId = attributes['deviceId'];

    const data = {

      Temperature_Living : payload.Temperature_Living,
      Temperature_Bedroom : payload.Temperature_Bedroom,
      Temperature_Kitchen : payload.Temperature_Kitchen,
      Humidity : payload.Humidity,
      Light_Living : payload.Light_Living,
      Light_Bedroom : payload.Light_Bedroom,
      Light_kitchen : payload.Light_kitchen,
      Light : payload.Light,
      CO2 : payload.CO2,
      HumidityRatio : payload.HumidityRatio,
      Occupancy_Living : payload.Occupancy_Living,
      Occupancy_Bedroom : payload.Occupancy_Bedroom,
      Occupancy_Kitchen : payload.Occupancy_Kitchen,
      Occupancy : payload.Occupancy,
      Door_Sensor : payload.Door_Sensor,
      device_id: payload.device_id,
      timecollected: payload.timecollected,
      Temperature : payload.Temperature

      // CO2: payload.CO2, 
      // Temperature_Living: payload.Temperature_Living,
      // Temperature_Bedroom: payload.Temperature_Bedroom,
      // Temperature_Kitchen: payload.Temperature_Kitchen,
      // Light_Living: payload.Light_Living,
      // Light_Bedroom: payload.Light_Bedroom,
      // Light_kitchen: payload.Light_kitchen,
      // Occupancy_Living: payload.Occupancy_Living,
      // Occupancy_Bedroom: payload.Occupancy_Bedroom,
      // Occupancy_Kitchen: payload.Occupancy_Kitchen,
      // Humidity: payload.Humidity, 
      // HumidityRatio: payload.HumidityRatio,
      // timecollected: payload.timecollected,
      // device_id: payload.device_id,
      // Door_Sensor: payload.Door_Sensor
    };
    return Promise.all([
      // insertIntoBigquery(data),
      updateCurrentDataFirebase(data)
    ]);

  });

/** 
 * Maintain last status in firebase
*/
function updateCurrentDataFirebase(data) {
  let ref = db.ref('/devices');
  let newRef = ref.child(data.device_id);
  return newRef.set({
    // CO2: data.CO2, 
    // Humidity: data.Humidity, 
    // HumidityRatio: data.HumidityRatio,
    // Light: data.Light,
    // Occupancy: data.Occupancy,
    // Temperature: data.Temperature,
    // device_id: data.device_id,
    // timecollected: data.timecollected  

    Temperature_Living : data.Temperature_Living,
    Temperature_Bedroom : data.Temperature_Bedroom,
    Temperature_Kitchen : data.Temperature_Kitchen,
    Humidity : data.Humidity,
    Light_Living : data.Light_Living,
    Light_Bedroom : data.Light_Bedroom,
    Light_kitchen : data.Light_kitchen,
    Light : data.Light,
    CO2 : data.CO2,
    HumidityRatio : data.HumidityRatio,
    Occupancy_Living : data.Occupancy_Living,
    Occupancy_Bedroom : data.Occupancy_Bedroom,
    Occupancy_Kitchen : data.Occupancy_Kitchen,
    Occupancy : data.Occupancy,
    Door_Sensor : data.Door_Sensor,
    device_id: data.device_id,
    timecollected: data.timecollected,
    Temperature : data.Temperature

  })
  // ref.once('value').then((snapshot) => {
  //   arr = snapshot ? snapshot : [];
  //   db.ref('/log').set(snapshot ?  JSON.stringify(snapshot) : 'notReceived');
  //   arr.push({
  //     CO2: data.CO2, 
  //     Humidity: data.Humidity, 
  //     HumidityRatio: data.HumidityRatio,
  //     Light: data.Light,
  //     Occupancy: data.Occupancy,
  //     Temperature: data.Temperature,
  //     device_id: data.device_id,
  //     timecollected: data.timecollected  
  //   })
  //   key = db.k
  //   return db.ref(`/devices`).set(arr);
  // }).catch(err => {
  //   console.log(err);
  // });

}

/**
 * Store all the raw data in bigquery
 */
function insertIntoBigquery(data) {
  // TODO: Make sure you set the `bigquery.datasetname` Google Cloud environment variable.
  const dataset = bigquery.dataset(functions.config().bigquery.datasetname);
  // TODO: Make sure you set the `bigquery.tablename` Google Cloud environment variable.
  const table = dataset.table(functions.config().bigquery.tablename);

  return table.insert(data);
}

/**
 * Query bigquery with the last 7 days of data
 * HTTPS endpoint to be used by the webapp
 */
exports.getReportData = functions.https.onRequest((req, res) => {
  const projectId = process.env.GCLOUD_PROJECT;
  const datasetName = functions.config().bigquery.datasetname;
  const tableName = functions.config().bigquery.tablename;
  const table = `${projectId}.${datasetName}.${tableName}`;

  const query = `
    SELECT 
      TIMESTAMP_TRUNC(data.timestamp, HOUR, 'America/Cuiaba') data_hora,
      avg(data.temp) as avg_temp,
      avg(data.humidity) as avg_hum,
      min(data.temp) as min_temp,
      max(data.temp) as max_temp,
      min(data.humidity) as min_hum,
      max(data.humidity) as max_hum,
      count(*) as data_points      
    FROM \`${table}\` data
    WHERE data.timestamp between timestamp_sub(current_timestamp, INTERVAL 7 DAY) and current_timestamp()
    group by data_hora
    order by data_hora
  `;

  /**return bigquery
    .query({
      query: query,
      useLegacySql: false
    })
    .then(result => {
      const rows = result[0];

      cors(req, res, () => {
        res.json(rows);
      });
    });*/
}); 